# Generación de APK desde la Terminal

A continuación, se describen los pasos para generar la APK de una aplicación utilizando la terminal:

1. **Navegar al directorio del proyecto:**
   - Abre la terminal y ve al directorio donde se encuentra tu proyecto.

2. **Abrir el proyecto en Visual Studio:**
   - Si aún no lo has hecho, abre tu proyecto en Visual Studio Code.
   - Luego, abre la consola en Visual Studio Code (puedes usar el atajo `Ctrl + Ñ`).

3. **Construir la aplicación:**
   - Ejecuta el siguiente comando para construir la aplicación:
     ```
     ionic build
     ```

4. **Agregar la plataforma Android:**
   - Si aún no has agregado la plataforma Android, ejecuta el siguiente comando:
     ```
     ionic capacitor add android
     ```

5. **Copiar los archivos y compilar la APK:**
   - Copia los archivos necesarios para la plataforma Android:
     ```
     ionic capacitor copy android
     ```
   - Luego, ve a la carpeta `android`:
     ```
     cd android
     ```
   - Compila la APK:
     ```
     ./gradlew assembleDebug
     ```
   - Regresa al directorio principal:
     ```
     cd ..
     ```

6. **Ubicación de la APK generada:**
   - La APK generada se encuentra en la siguiente ruta:
     ```
     android/app/build/outputs/apk/debug/app-debug.apk
     ```

