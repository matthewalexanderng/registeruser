import { createAction, props } from '@ngrx/store';

export const saveFormData = createAction(
  '[Form] Save Form Data',
  props<{ data: any }>()
);
