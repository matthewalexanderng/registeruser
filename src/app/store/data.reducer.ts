import { createReducer, on } from '@ngrx/store';
import { saveFormData } from './data.actions';

export interface State {
  formData: any;
}

export const initialState: State = {
  formData: null
};

const _dataReducer = createReducer(
  initialState,
  on(saveFormData, (state, { data }) => ({
    ...state,
    formData: data
  }))
);

export function dataReducer(state: any, action: any) {
  return _dataReducer(state, action);
}
