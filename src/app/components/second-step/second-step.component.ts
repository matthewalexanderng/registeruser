import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { saveFormData } from 'src/app/store/data.actions';

@Component({
  selector: 'app-second-step',
  templateUrl: './second-step.component.html',
  styleUrls: ['./second-step.component.scss'],
})
export class SecondStepComponent implements OnInit {


  form: FormGroup;
  documentTypes: any[] = [];
  genders: any[] = [];
  loading: boolean = false;
  showDocumentDatePicker: boolean = false;
  showBirthDatePicker: boolean = false;
  emailSame: boolean = true

  constructor(
    private router: Router,
    private store: Store,
    private fb: FormBuilder,
    private http: HttpClient,
    private alertController: AlertController,
    private loadingController: LoadingController) {
    this.next = new EventEmitter();
    this.form = this.fb.group({
      documentType: ['', Validators.required],
      documentNumber: ['', [Validators.required, Validators.pattern(/^\d{10}$/)]],
      documentDate: ['', Validators.required],
      birthDate: ['', Validators.required],
      gender: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required, Validators.email]],
      pin: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
      confirmPin: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]]

    }, {
      validators: [this.emailMatchValidator, this.pinMatchValidator]
    });
  }



  @Output() next: EventEmitter<number>;

  ngOnInit() {
    this.loadDocumentTypes();
    this.loadGenders();
  }

  nextStep() {
    console.log('siguiente step, 3');

  }


  async loadDocumentTypes() {
    const loading = await this.loadingController.create({
      message: 'Cargando tipos de documentos...',
    });
    await loading.present();

    this.http.get('https://api.bancoink.biz/qa/signup/documentTypes?apiKey=030106').subscribe(
      (data: any) => {
        this.documentTypes = data;
        loading.dismiss();
      },
      async (error) => {
        loading.dismiss();
        const alert = await this.alertController.create({
          header: 'Error',
          message: 'No se pudieron cargar los tipos de documentos.',
          buttons: ['OK']
        });
        await alert.present();
      }
    );
  }

  onDateSelected(event: any, controlName: string) {
    this.form.get(controlName)?.setValue(event.detail.value);
    if (controlName === 'documentDate') {
      this.showDocumentDatePicker = false;
    } else if (controlName === 'birthDate') {
      this.showBirthDatePicker = false;
    }
  }



  async loadGenders() {
    const loading = await this.loadingController.create({
      message: 'Cargando géneros...',
    });
    await loading.present();

    this.http.get('https://api.bancoink.biz/qa/signup/genders?apiKey=030106').subscribe(
      (data: any) => {
        this.genders = [{
          id: 1,
          name: 'Hombre'
        },
        {
          id: 2,
          name: 'Mujer'
        },
        {
          id: 3,
          name: 'No binarie'
        }
        ];
        loading.dismiss();
      },
      async (error) => {
        loading.dismiss();
        const alert = await this.alertController.create({
          header: 'Error',
          message: 'No se pudieron cargar los géneros.',
          buttons: ['OK']
        });
        await alert.present();
      }
    );
  }

  emailMatchValidator(group: FormGroup) {
    const email = group.get('email')?.value;
    const confirmEmail = group.get('confirmEmail')?.value;

    return email === confirmEmail ? null : { emailMismatch: true };
  }

  pinMatchValidator(group: FormGroup) {
    const pin = group.get('pin')?.value;
    const confirmPin = group.get('confirmPin')?.value;
    return pin === confirmPin ? null : { pinMismatch: true };
  }

  togglePasswordVisibility(inputName: string) {
    const input = document.querySelector(`ion-input[formControlName=${inputName}]`);
    const inputType = input?.getAttribute('type');
    input?.setAttribute('type', inputType === 'password' ? 'text' : 'password');
  }

  async onSubmit() {
    if (this.form.invalid) {
      const alert = await this.alertController.create({
        header: 'Error',
        message: 'Por favor complete todos los campos correctamente.',
        buttons: ['OK']
      });
      await alert.present();
      return;
    }

    this.store.dispatch(saveFormData({ data: this.form.value }));

    const alert = await this.alertController.create({
      header: 'Éxito',
      message: 'Se guardaron los datos de manera efectiva.'
    });
    await alert.present();

    setTimeout(() => {
      this.loading = false;
      console.log('Datos guardados:', this.form.value);
      alert.dismiss()
      this.next.emit(3)
    }, 6000);
  }
}
