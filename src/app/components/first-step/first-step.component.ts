import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.scss'],
})
export class FirstStepComponent {
  value: string = '';

  constructor() {
    this.next = new EventEmitter();
  }

  @Output() next: EventEmitter<number>;

  get formattedValue() {
    // Formatea el valor como (XXX - XXXXXX)
    let formatted = this.value;
    if (this.value.length > 3) {
      formatted = `${this.value.slice(0, 3)} - ${this.value.slice(3)}`;
    }
    return formatted;
  }

  onNumberClicked(num: number) {
    if (this.value.length < 9) { // Limita la longitud del input a 9 caracteres
      this.value += num;
    }
  }

  onClearClicked() {
    if (this.value.length > 0) {
      this.value = this.value.slice(0, -1);
    }
  }

  onConfirmClicked() {
    const valid = this.validateValue(this.value);
    if (valid) {
      // alert('Formato válido');
      this.next.emit(2)
      } else {
      this.next.emit(2)
      // alert('Formato inválido');
    }
  }

  validateValue(value: string) {
    // Asegúrate de que el valor tenga exactamente 9 caracteres numéricos
    const regex = /^\d{9}$/;
    return regex.test(value);
  }
}
