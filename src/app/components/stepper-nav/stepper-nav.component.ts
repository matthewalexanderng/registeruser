import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-stepper-nav',
  templateUrl: './stepper-nav.component.html',
  styleUrls: ['./stepper-nav.component.scss'],
})
export class StepperNavComponent {

  constructor() { }
  buttonActive: string = 'ProductsComponent';
  stepperTwo: boolean = false;
  stepperThree: boolean = false;

  //Variable de number total de paginas del stepper enviado desde el padre
  @Input()
  activeStepper!: number;
  @Input() stepperTotales!: number;
  @Input() stateItemStepper!: string;

}
