import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-num-pad',
  templateUrl: './num-pad.component.html',
  styleUrls: ['./num-pad.component.scss'],
})
export class NumPadComponent {

  @Output() numberClicked = new EventEmitter<number>();
  @Output() clearClicked = new EventEmitter<void>();
  @Output() confirmClicked = new EventEmitter<void>();

  numPad = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'clear', 0, 'confirm'];
  
  constructor() { }

  onNumberClick(num: any) {
    if (num === 'clear') {
      this.clearClicked.emit();
    } else if (num === 'confirm') {
      this.confirmClicked.emit();
    } else {
      this.numberClicked.emit(num);
    }
  }

}

