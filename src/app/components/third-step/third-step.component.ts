import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-third-step',
  templateUrl: './third-step.component.html',
  styleUrls: ['./third-step.component.scss'],
})
export class ThirdStepComponent  {

  statusChecked : boolean = false

  constructor(private router: Router) { }
  checkStatus(status:any){
    this.statusChecked = status.detail.checked 
  }

  finishProcess(){
    if(this.statusChecked){
      this.router.navigate(['/end-page'])
    } else{
      alert('No data')
    }
  }

}
