import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';

import { ContentPagePageRoutingModule } from './content-page-routing.module';

import { ContentPagePage } from './content-page.page';
import { StepperNavComponent } from 'src/app/components/stepper-nav/stepper-nav.component';
import { FirstStepComponent } from 'src/app/components/first-step/first-step.component';
import { SecondStepComponent } from 'src/app/components/second-step/second-step.component';
import { ThirdStepComponent } from 'src/app/components/third-step/third-step.component';
import { NumPadComponent } from 'src/app/components/num-pad/num-pad.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContentPagePageRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    ContentPagePage,
    StepperNavComponent,
    FirstStepComponent,
    SecondStepComponent,
    ThirdStepComponent,
    NumPadComponent
  ]
})
export class ContentPagePageModule { }
