import { Component } from '@angular/core';

@Component({
  selector: 'app-content-page',
  templateUrl: './content-page.page.html',
  styleUrls: ['./content-page.page.scss'],
})
export class ContentPagePage {

  constructor() { }
  stepperInicial: number = 1;
  steppersTotales: number = 3;
  stateStepper: string = "";

  getStep(step: number) {
    this.stepperInicial = step
  }

}
