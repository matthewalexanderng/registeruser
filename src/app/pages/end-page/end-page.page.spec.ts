import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EndPagePage } from './end-page.page';

describe('EndPagePage', () => {
  let component: EndPagePage;
  let fixture: ComponentFixture<EndPagePage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(EndPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
