import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EndPagePage } from './end-page.page';

const routes: Routes = [
  {
    path: '',
    component: EndPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EndPagePageRoutingModule {}
