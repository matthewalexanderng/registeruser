import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EndPagePageRoutingModule } from './end-page-routing.module';

import { EndPagePage } from './end-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EndPagePageRoutingModule
  ],
  declarations: [EndPagePage]
})
export class EndPagePageModule {}
