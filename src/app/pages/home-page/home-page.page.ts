import { Component, OnInit } from '@angular/core';
// import coinkLogo from "../../../assets/svg/coinkLogo.svg"
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.page.html',
  styleUrls: ['./home-page.page.scss'],
})
export class HomePagePage implements OnInit {

  load: boolean = true
  constructor() { }
  
  ngOnInit() {
    if (this.load) {
      setTimeout(() => {
        this.load = false
      }, 5000);
    }
  }

  // coinkLogo : any = coinkLogo
  userRegister() {
    console.log('Registrando');
  }

}
