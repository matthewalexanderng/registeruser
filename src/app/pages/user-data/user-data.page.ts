import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.page.html',
  styleUrls: ['./user-data.page.scss'],
})
export class UserDataPage {
  formData$: Observable<any>;
  
  constructor(private store: Store<{ data: { formData: any } }>) {
    this.formData$ = this.store.pipe(select('data', 'formData'));
  }


}
